﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PuzzleController))]
public class PuzzleEditor : Editor
{



    public override void OnInspectorGUI()
    {
        PuzzleController puzzle = (PuzzleController)target;

        if (GUILayout.Button("Clean Up Connections"))
        {
            puzzle.Data.CleanUpConnections();
        }

        if (GUILayout.Button("Mirror X"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(-puzzle.Data.Verticies[i].x, puzzle.Data.Verticies[i].y, puzzle.Data.Verticies[i].z);
            }
        }
        if (GUILayout.Button("Mirror Y"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(puzzle.Data.Verticies[i].x, -puzzle.Data.Verticies[i].y, puzzle.Data.Verticies[i].z);
            }
        }
        if (GUILayout.Button("Mirror Z"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(puzzle.Data.Verticies[i].x, puzzle.Data.Verticies[i].y, -puzzle.Data.Verticies[i].z);
            }
        }

        if (GUILayout.Button("Swap X and Y"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(puzzle.Data.Verticies[i].y, puzzle.Data.Verticies[i].x, puzzle.Data.Verticies[i].z);
            }
        }
        if (GUILayout.Button("Swap X and Z"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(puzzle.Data.Verticies[i].z, puzzle.Data.Verticies[i].y, puzzle.Data.Verticies[i].x);
            }
        }
        if (GUILayout.Button("Swap Y and Z"))
        {
            for (int i = 0; i < puzzle.Data.Verticies.Length; i++)
            {
                puzzle.Data.Verticies[i] = new Vector3(puzzle.Data.Verticies[i].x, puzzle.Data.Verticies[i].z, puzzle.Data.Verticies[i].y);
            }
        }

        DrawDefaultInspector();

    }
}
