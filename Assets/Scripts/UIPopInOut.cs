﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPopInOut : MonoBehaviour
{
    [SerializeField] GameObject Target;
    [SerializeField] float delay;

    public void Animate()
    {
        StopAllCoroutines();
        Target.SetActive(true);
        StartCoroutine(PopOut());
    }

    private IEnumerator PopOut()
    {
        yield return new WaitForSecondsRealtime(delay);
        Target.SetActive(false);
    }


}
