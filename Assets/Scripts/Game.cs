﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BasicInput;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{

    [SerializeField] BasicInput.BasicInput Input;
    [SerializeField] int PuzzlesInLevel;
    [SerializeField] float RestartDelay;


    private void Start()
    {
        Input = new BasicInput.BasicInput();
        Input.Enable();
        Input.InGame.Interaction.performed += _ => HideCursor();
        Input.InGame.Escape.performed += _ => ShowCursor();
    }

    public static void HideCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public static void ShowCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            HideCursor();
        }
        else
        {
            ShowCursor();
        }
    }

    public void CountFinishedLevels()
    {
        PuzzlesInLevel--;
        if (PuzzlesInLevel == 0)
        {
            StartCoroutine(RestartLevel());
        }
    }

    private IEnumerator RestartLevel()
    {
        yield return new WaitForSecondsRealtime(RestartDelay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

}
