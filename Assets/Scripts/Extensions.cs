﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
	public static bool ListContainsConnection(this List<Vector2Int> list, Vector2Int conn)
	{
		for (int i = 0; i < list.Count; i++)
		{
			var listItem = (list[i].x, list[i].y);
			if (listItem == (conn.x, conn.y) || listItem == (conn.y, conn.x))
			{
				return true;
			}
		}
		return false;
	}

	public static bool ListContainsInt(this List<Vector2Int> list, int toFind)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].x == toFind || list[i].y == toFind)
			{
				return true;
			}
		}
		return false;
	}
}
