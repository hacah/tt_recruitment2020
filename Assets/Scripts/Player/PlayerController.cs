﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BasicInput;
using System;

public class PlayerController : MonoBehaviour
{

    BasicInput.BasicInput Input;
    [SerializeField] float MovementSpeed;
    [SerializeField] float MouseSensitivity;
    [SerializeField] Rigidbody RB;
    [SerializeField] Camera Camera;

    private int RaycastSkip = 6;
    private int RaycastUpdatesSkiped = 0;

    private float RaycastDistance = 30f;
    private int ScarabsLayer = 1 << 8;

    ScarabController HitScarab;

    // Start is called before the first frame update
    void Start()
    {
        Input = new BasicInput.BasicInput();
        Input.Enable();
        Input.InGame.Interaction.performed += _ => Interact();
        Input.InGame.SecondaryInteraction.performed += _ => SecondaryInteraction();
    }
    
    void Update()
    {
        Move();
        Rotate();
    }

    private void FixedUpdate()
    {
        if (RaycastUpdatesSkiped >= RaycastSkip)
        {
            ScarabController PreviousScarab = HitScarab;
            HitScarab = RaycastScarab();
            if (PreviousScarab != null && HitScarab != PreviousScarab)
            {
                PreviousScarab.RemoveHighlight();
            }
            HighlightScarab();
            RaycastUpdatesSkiped = 0;
        }
        else
        {
            RaycastUpdatesSkiped++;
        }
        
    }

    private void HighlightScarab()
    {
        if (HitScarab == null)
        {
            return;
        }

        HitScarab.Highlight();
    }

    void Move()
    {
        Vector2 value = Input.InGame.Movement.ReadValue<Vector2>() * Time.deltaTime * MovementSpeed;
        float x = value.y * Mathf.Sin(Mathf.Deg2Rad * transform.eulerAngles.y) + value.x * Mathf.Cos(Mathf.Deg2Rad * transform.eulerAngles.y);
        float z = -value.x * Mathf.Sin(Mathf.Deg2Rad * transform.eulerAngles.y) + value.y * Mathf.Cos(Mathf.Deg2Rad * transform.eulerAngles.y);
        RB.velocity = new Vector3(x, RB.velocity.y, z);
    }

    void Rotate()
    {
        if (Cursor.visible)
        {
            return;
        }
        Vector2 value = Input.InGame.Rotation.ReadValue<Vector2>() * MouseSensitivity;
        transform.eulerAngles += new Vector3(0f,value.x,0f);
        float cameraYaw = Camera.transform.localEulerAngles.x;
        if (cameraYaw > 200f)
        {
            cameraYaw = Mathf.Clamp(cameraYaw - value.y, 271f, float.PositiveInfinity);
        }
        else
        {
            cameraYaw = Mathf.Clamp(cameraYaw - value.y, float.NegativeInfinity, 89f);
        }
        
        Camera.transform.localEulerAngles = new Vector3(cameraYaw,0f,0f);
    }

    void Interact()
    {
        if (Cursor.visible || HitScarab == null)
        {
            return;
        }

        HitScarab.Interact();        
    }

    void SecondaryInteraction()
    {
        if (Cursor.visible || HitScarab == null)
        {
            return;
        }

        HitScarab.SecondaryInteraction();
    }

    ScarabController RaycastScarab()
    {
        Physics.Raycast(Camera.transform.position, Camera.transform.forward, out RaycastHit hit, RaycastDistance, ScarabsLayer);

        if (hit.collider != null)
        {
            return hit.collider.gameObject.GetComponent<ScarabController>();
        }
        else {
            return null;
        }
    }
}
