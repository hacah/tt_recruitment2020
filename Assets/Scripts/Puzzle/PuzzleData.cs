﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PuzzleData
{
	//Using Arrays because they are visible in the default inspector
	[SerializeField] public Vector3[] Verticies;
	[SerializeField] public Vector2Int[] Connections;

	public void CleanUpConnections()
	{
		RemoveConnectionsToSelf();
		RemoveOutOfBoundsConnections();
		RemoveDuplicateConnections();
	}

	public void RemoveDuplicateConnections()
	{
		List<Vector2Int> GoodConections = new List<Vector2Int>();

		for (int i = 0; i < Connections.Length; i++)
		{
			if (!GoodConections.ListContainsConnection(Connections[i]))
			{
				GoodConections.Add(Connections[i]);
			}
		}
		Connections = GoodConections.ToArray();
	}

	public void RemoveOutOfBoundsConnections()
	{
		List<Vector2Int> GoodConnections = new List<Vector2Int>();
		for (int i = 0; i < Connections.Length; i++)
		{
			if (Connections[i].x >= 0 && Connections[i].x < Verticies.Length &&
				Connections[i].y >= 0 && Connections[i].y < Verticies.Length
				)
			{
				GoodConnections.Add(Connections[i]);
			}
		}
		Connections = GoodConnections.ToArray();
	}

	public void RemoveConnectionsToSelf()
	{
		List<Vector2Int> GoodConnections = new List<Vector2Int>();
		for (int i = 0; i < Connections.Length; i++)
		{
			if (Connections[i].x != Connections[i].y)
			{
				GoodConnections.Add(Connections[i]);
			}
		}
		Connections = GoodConnections.ToArray();
	}

	public List<int> ConnectedVerticies(int root)
	{
		List<int> ConnectedVerticies = new List<int>();
		for (int i = 0; i < Connections.Length; i++)
		{
			if (Connections[i].x == root)
			{
				ConnectedVerticies.Add(Connections[i].y);
			}
			else if (Connections[i].y == root)
			{
				ConnectedVerticies.Add(Connections[i].x);
			}
		}
		return ConnectedVerticies;
	}


}
