﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleController : MonoBehaviour
{
    [SerializeField] float VertexGizmoSize = 0.2f;
    [SerializeField] public PuzzleData Data;
    [SerializeField] GameObject ScarabPrefab;
    [SerializeField] UIPopInOut FinishedText;
    [SerializeField] Game Game;
    [Header("Line")]
    [SerializeField] LineRenderer Line;
    [SerializeField] Material DefaultLineMaterial;
    [SerializeField] Material FinishedLineMaterial;
    [Header("Audio")]
    [SerializeField] AudioSource Audio;
    [SerializeField] AudioClip Reset;
    [SerializeField] AudioClip Finished;
    private List<ScarabController> Scarabs = new List<ScarabController>();
    public Stack<int> Moves = new Stack<int>();
    private bool _isPuzzleFinished = false;

    private float ScarabsRotationTime = 2f;

    [HideInInspector] public bool IsPuzzleFinished
    {
        get
        {
            return _isPuzzleFinished;
        }
        private set
        {
            _isPuzzleFinished = value;
        }
    }

    private void Start()
    {
        Data.CleanUpConnections();
        SpawnScarabs();
    }

    public void OnDrawGizmosSelected()
    {
        for (int i = 0; i < Data.Verticies.Length; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(GetVertexWorldPosition(i),VertexGizmoSize);
        }
        for (int i = 0; i < Data.Connections.Length; i++)
        {
            Gizmos.color = Color.blue;
            try
            {
                Gizmos.DrawLine(GetVertexWorldPosition(Data.Connections[i].x), GetVertexWorldPosition(Data.Connections[i].y));
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("Could not draw Connections: " + e.Message);
            }
        }
    }

    Vector3 GetVertexWorldPosition(int index)
    {
        if (index < 0 || index > Data.Verticies.Length - 1)
        {
            Debug.LogError("Index Outside the bounds of the Data");
            throw new System.IndexOutOfRangeException();            
        }
        else
        {
            return Data.Verticies[index] + transform.position;
        }

    }

    public void AddClickedScarab(int index, bool PlaySound = true)
    {
        if (ConnectionsAvailable(index) && CanConnectTo(index))
        {
            for (int i = 0; i < Scarabs.Count; i++)
            {
                Scarabs[i].State.InRange = false;
                Scarabs[i].State.Current = (i == index);
            }
            Scarabs[index].State.Clicked = true;

            if (PlaySound)
            {
                Scarabs[index].PlayClickSound();
            }

            int previous = -1;

            if (Moves.Count > 0)
            {
                previous = Moves.Peek();
            }
            else
            {
                Scarabs[index].State.First = true;
            }

            Moves.Push(index);

            List<int> ConnectedVerticies = Data.ConnectedVerticies(index);
            for (int i = 0; i < ConnectedVerticies.Count; i++)
            {
                if (ConnectedVerticies[i] != previous && CanConnectTo(ConnectedVerticies[i]))
                {
                    Scarabs[ConnectedVerticies[i]].State.InRange = true;
                }
                else
                {
                    Scarabs[ConnectedVerticies[i]].State.InRange = false;
                }
            }
        }

        if (ConnectionsMade().Count == Data.Connections.Length)
        {
            if (PuzzleFinished())
            {
                FinishPuzzle();
            }
        }
        else
        {
            Line.material = DefaultLineMaterial;
        }

        DrawLine();

        for (int i = 0; i < Scarabs.Count; i++)
        {
            Scarabs[i].Refresh();
        }
    }

    public void ReturnOneMove()
    {
        if (Moves.Count == 1)
        {
            ResetPuzzle();
            return;
        }

        int toDisableIndex = Moves.Pop();
        ScarabController toDisable = Scarabs[toDisableIndex];

        toDisable.State.Current = false;
        toDisable.State.InRange = true;
        toDisable.PlayBackSound();

        if (!ConnectionsMade().ListContainsInt(toDisableIndex))
        {
            toDisable.State.Clicked = false;
        }

        toDisable.Refresh();

        AddClickedScarab(Moves.Pop(), false);
    }

    public void ResetPuzzle()
    {
        Audio.PlayOneShot(Reset);
        Moves = new Stack<int>();
        DestroyScarabs();
        SpawnScarabs();
        DrawLine();
    }

    public List<Vector2Int> ConnectionsMade()
    {
        List<Vector2Int> result = new List<Vector2Int>();

        int[] moves = Moves.ToArray();

        for (int i = 1; i < moves.Length; i++)
        {
            result.Add(new Vector2Int(moves[i-1],moves[i]));
        }

        return result;
    }

    public void SpawnScarabs()
    {
        Scarabs = new List<ScarabController>();
        for (int i = 0; i < Data.Verticies.Length; i++)
        {
            GameObject Scarab = Instantiate(ScarabPrefab, GetVertexWorldPosition(i), this.transform.rotation, this.transform);
            Vector3 scale = new Vector3(
                transform.localScale.x / transform.lossyScale.x * ScarabPrefab.transform.localScale.x,
                transform.localScale.y / transform.lossyScale.y * ScarabPrefab.transform.localScale.y,
                transform.localScale.z / transform.lossyScale.z * ScarabPrefab.transform.localScale.z);
            Scarab.transform.localScale = scale;
            Scarab.transform.localEulerAngles = ScarabPrefab.transform.localEulerAngles;

            ScarabController controller = Scarab.GetComponent<ScarabController>();
            controller.Puzzle = this;
            controller.index = i;

            Scarabs.Add(controller);
        }
    }

    public void DestroyScarabs()
    {
        for (int i = 0; i < Scarabs.Count; i++)
        {
            Scarabs[i].KillScarab();
        }
        Scarabs = new List<ScarabController>();
    }

    public void DrawLine()
    {
        List<Vector3> Points = new List<Vector3>();
        int[] moves = Moves.ToArray();

        for (int i = 0; i < moves.Length; i++)
        {
            Points.Add(GetVertexWorldPosition(moves[i]));
        }

        Line.positionCount = Points.Count;
        Line.SetPositions(Points.ToArray());
    }

    bool ConnectionsAvailable(int index)
    {
        int StartingConnectionsCount = Data.ConnectedVerticies(index).Count;
        int MadeConnections = 0;

        foreach (var item in ConnectionsMade())
        {
            if (item.x == index || item.y == index)
            {
                MadeConnections += 1;
            }
        }

        return StartingConnectionsCount > MadeConnections;
    }

    bool CanConnectTo(int to)
    {
        if (Moves.Count > 0)
        {
            return CanConnectTo(to, Moves.Peek());
        }
        else
        {
            return true;
        }
    }

    bool CanConnectTo(int to, int from)
    {
        List<int> StartingConnections = Data.ConnectedVerticies(to);

        if (!StartingConnections.Contains(from))
        {
            return false;
        }

        List<Vector2Int> MadeConnections = new List<Vector2Int>();

        foreach (var item in ConnectionsMade())
        {
            if (item.x == to || item.y == to)
            {
                MadeConnections.Add(item);
            }
        }

        for (int i = 0; i < MadeConnections.Count; i++)
        {
            (int x, int y) conn = (MadeConnections[i].x, MadeConnections[i].y);
            if (conn == (to,from) || conn == (from,to))
            {
                return false; //Already Connected
            }
        }

        return true;
    }

    public bool PuzzleFinished()
    {
        List<Vector2Int> MadeConnections = ConnectionsMade();
        List<Vector2Int> ConnectionsToFinish = new List<Vector2Int>(Data.Connections);

        for (int i = 0; i < ConnectionsToFinish.Count; i++)
        {
            (int x, int y) ConnectionToFinish = (ConnectionsToFinish[i].x, ConnectionsToFinish[i].y);
            bool Found = false;
            for (int j = 0; j < MadeConnections.Count; j++)
            {
                (int x, int y) MadeConnection = (MadeConnections[j].x, MadeConnections[j].y);
                if (ConnectionToFinish == (MadeConnection.x, MadeConnection.y) || 
                    ConnectionToFinish == (MadeConnection.y, MadeConnection.x))
                {
                    Found = true;
                    MadeConnections.RemoveAt(j);
                    break;
                }
            }
            if (!Found)
            {
                return false;
            }
        }

        return true;
    }

    public void RotateScarabsToSelectedScarab(int selectedIndex, float time)
    {
        if (time <= 0)
        {
            Debug.LogError("Rotation time cannot be negative");
            time = ScarabsRotationTime;
        }
        Transform selectedScarab = Scarabs[selectedIndex].transform;
        for (int i = 0; i < Scarabs.Count; i++)
        {
            if (i != selectedIndex)
            {
                Scarabs[i].SmoothlyRotateTowards(selectedScarab, time);
            }
        }
    }

    public void RotateScarabsToSelectedScarab(int selectedIndex)
    {
        RotateScarabsToSelectedScarab(selectedIndex, ScarabsRotationTime);
    }

    private void FinishPuzzle()
    {
        IsPuzzleFinished = true;
        Line.material = FinishedLineMaterial;
        Debug.Log("Finished Puzzle");
        Audio.PlayOneShot(Finished);

        for (int i = 0; i < Scarabs.Count; i++)
        {
            Scarabs[i].State.Current = true;
            Scarabs[i].State.InRange = false;
            Scarabs[i].State.First = false;
        }

        FinishedText.Animate();

        Game.CountFinishedLevels();

    }

    
}
