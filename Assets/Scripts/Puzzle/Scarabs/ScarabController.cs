﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScarabController : MonoBehaviour
{
    public PuzzleController Puzzle;
    public int index;
    public ScarabState State = new ScarabState();

    [Header("Particles")]
    public ParticleSystem HighlightedParticles;
    private ParticleSystem.EmissionModule highlightedEmission;

    public ParticleSystem CurrentParticles;
    private ParticleSystem.EmissionModule currentEmission;

    public ParticleSystem FirstParticles;
    private ParticleSystem.EmissionModule firstEmission;

    public ParticleSystem InRangeParticles;
    private ParticleSystem.EmissionModule inRangeEmission;

    public ParticleSystem ClickedParticles;
    private ParticleSystem.EmissionModule clickedEmission;

    [Header("Renderers")]
    public GameObject StoneRenderer;
    public GameObject BlueRenderer;
    public GameObject GoldRenderer;

    [Header("Audio")]
    [SerializeField] AudioSource Audio;
    [SerializeField] AudioClip Click;
    [SerializeField] AudioClip Back;

    private Coroutine SmoothRotation;

    private void Start()
    {
        firstEmission = FirstParticles.emission;
        currentEmission = CurrentParticles.emission;
        clickedEmission = ClickedParticles.emission;
        highlightedEmission = HighlightedParticles.emission;
        inRangeEmission = InRangeParticles.emission;

        firstEmission.enabled = false;
        currentEmission.enabled = false;
        clickedEmission.enabled = false;
        highlightedEmission.enabled = false;
        inRangeEmission.enabled = false;
    }

    public void Highlight()
    {
        if (!Puzzle.IsPuzzleFinished)
        {
            try
            {
                if (!highlightedEmission.enabled)
                {
                    Puzzle.RotateScarabsToSelectedScarab(this.index);
                }
                highlightedEmission.enabled = true;
            }
            catch (System.Exception e)
            {
                Debug.Log("Could not enable highlight: " + e.Message);
            }
        }
    }

    public void RemoveHighlight()
    {
        try
        {
            highlightedEmission.enabled = false;
        }
        catch (System.Exception e)
        {
            Debug.Log("Could not disable highlight: " + e.Message);
        }
        
    }

    public void Interact()
    {
        if (!Puzzle.IsPuzzleFinished)
        {
            if (Puzzle.Moves.Count == 0)
            {
                Puzzle.AddClickedScarab(index);
            }
            else if (State.Current)
            {
                Puzzle.ReturnOneMove();
            }
            else if (State.InRange)
            {
                Puzzle.AddClickedScarab(index);
            }
        }
        Refresh();
    }

    public void SecondaryInteraction()
    {
        if (!Puzzle.IsPuzzleFinished)
        {
            if (State.First)
            {
                Puzzle.ResetPuzzle();
            }
        }
    }

    void EmitParticles()
    {
        firstEmission.enabled = State.First;
        inRangeEmission.enabled = State.InRange;
        if (State.Current)
        {
            currentEmission.enabled = true;
            clickedEmission.enabled = false;
        }
        else if (State.Clicked)
        {
            currentEmission.enabled = false;
            clickedEmission.enabled = true;
        }
        else
        {
            currentEmission.enabled = false;
            clickedEmission.enabled = false;
        }
    }

    public void Refresh()
    {
        EmitParticles();
        if (State.Current)
        {
            GoldRenderer.SetActive(true);
            BlueRenderer.SetActive(false);
            StoneRenderer.SetActive(false);
        }
        else if (State.Clicked)
        {
            GoldRenderer.SetActive(false);
            BlueRenderer.SetActive(true);
            StoneRenderer.SetActive(false);
        }
        else
        {
            GoldRenderer.SetActive(false);
            BlueRenderer.SetActive(false);
            StoneRenderer.SetActive(true);
        }
    }

    public void KillScarab()
    {
        Destroy(this.gameObject);
    }

    public void SmoothlyRotateTowards(Transform target, float time)
    {
        if (SmoothRotation != null)
        {
            StopCoroutine(SmoothRotation);
        }
        SmoothRotation = StartCoroutine(Rotate(target, time));
    }

    private IEnumerator Rotate(Transform target, float time)
    {
        float timePassed = 0f;
        Vector3 targetDirection = transform.position - target.position;
        Vector3 currentRotation = StoneRenderer.transform.up;
        while (timePassed < time)
        {
            timePassed += Time.deltaTime;

            RotateRenderers(Vector3.Lerp(currentRotation,targetDirection, timePassed/time));

            yield return new WaitForEndOfFrame();
        }
        RotateRenderers(targetDirection);
        SmoothRotation = null;
    }

    private void RotateRenderers(Vector3 Rotation)
    {
        StoneRenderer.transform.up = Rotation;
        BlueRenderer.transform.up = Rotation;
        GoldRenderer.transform.up = Rotation;
    }

    public void PlayClickSound()
    {
        Audio.PlayOneShot(Click);
    }

    public void PlayBackSound()
    {
        Audio.PlayOneShot(Back);
    }

}
