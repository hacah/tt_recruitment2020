﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScarabState
{
    public bool Current = false; //gold
    public bool First = false;
    public bool Clicked = false; //blue
    public bool InRange = false;
}
