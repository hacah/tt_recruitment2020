// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input/BasicInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace BasicInput
{
    public class @BasicInput : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @BasicInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""BasicInput"",
    ""maps"": [
        {
            ""name"": ""InGame"",
            ""id"": ""a3f8ed47-212a-499d-a959-887a5ef421dd"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""62dab411-cd3b-42eb-8e60-88e0921d072e"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rotation"",
                    ""type"": ""Value"",
                    ""id"": ""7160f1f1-5668-436b-93c7-70815c39cd71"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interaction"",
                    ""type"": ""Button"",
                    ""id"": ""dad6e118-97c2-49e3-87a7-bfd974230018"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Escape"",
                    ""type"": ""Button"",
                    ""id"": ""29675ca5-be8e-4cfa-87b8-397030813cd3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondaryInteraction"",
                    ""type"": ""Button"",
                    ""id"": ""def73d7a-5ac0-42ac-9bc7-3c1da1389035"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WSAD"",
                    ""id"": ""0c7da482-1cef-4d83-b657-a82fa9814574"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""9f45066a-229c-482c-bc57-7ffd2b231492"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""5633af46-c569-4454-b76c-5dea12eb263e"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""7a54369d-c7c6-471a-ae23-70532d7ffc50"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""2f576aeb-e74d-409e-b7df-0a814950e250"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5dc1e211-bb4f-48ca-a518-27bfd2b45d65"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3ffc8dfa-99c9-4c13-8374-30fad4b61ca1"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6eeb25cc-c186-4a3d-9c92-513e3e48f7e8"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f3fd4c09-4e58-4f18-b9a7-989ac3c1d226"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Hold"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SecondaryInteraction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardAndMouse"",
            ""bindingGroup"": ""KeyboardAndMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // InGame
            m_InGame = asset.FindActionMap("InGame", throwIfNotFound: true);
            m_InGame_Movement = m_InGame.FindAction("Movement", throwIfNotFound: true);
            m_InGame_Rotation = m_InGame.FindAction("Rotation", throwIfNotFound: true);
            m_InGame_Interaction = m_InGame.FindAction("Interaction", throwIfNotFound: true);
            m_InGame_Escape = m_InGame.FindAction("Escape", throwIfNotFound: true);
            m_InGame_SecondaryInteraction = m_InGame.FindAction("SecondaryInteraction", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // InGame
        private readonly InputActionMap m_InGame;
        private IInGameActions m_InGameActionsCallbackInterface;
        private readonly InputAction m_InGame_Movement;
        private readonly InputAction m_InGame_Rotation;
        private readonly InputAction m_InGame_Interaction;
        private readonly InputAction m_InGame_Escape;
        private readonly InputAction m_InGame_SecondaryInteraction;
        public struct InGameActions
        {
            private @BasicInput m_Wrapper;
            public InGameActions(@BasicInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Movement => m_Wrapper.m_InGame_Movement;
            public InputAction @Rotation => m_Wrapper.m_InGame_Rotation;
            public InputAction @Interaction => m_Wrapper.m_InGame_Interaction;
            public InputAction @Escape => m_Wrapper.m_InGame_Escape;
            public InputAction @SecondaryInteraction => m_Wrapper.m_InGame_SecondaryInteraction;
            public InputActionMap Get() { return m_Wrapper.m_InGame; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(InGameActions set) { return set.Get(); }
            public void SetCallbacks(IInGameActions instance)
            {
                if (m_Wrapper.m_InGameActionsCallbackInterface != null)
                {
                    @Movement.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnMovement;
                    @Movement.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnMovement;
                    @Movement.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnMovement;
                    @Rotation.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnRotation;
                    @Rotation.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnRotation;
                    @Rotation.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnRotation;
                    @Interaction.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnInteraction;
                    @Interaction.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnInteraction;
                    @Interaction.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnInteraction;
                    @Escape.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnEscape;
                    @Escape.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnEscape;
                    @Escape.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnEscape;
                    @SecondaryInteraction.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnSecondaryInteraction;
                    @SecondaryInteraction.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnSecondaryInteraction;
                    @SecondaryInteraction.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnSecondaryInteraction;
                }
                m_Wrapper.m_InGameActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Movement.started += instance.OnMovement;
                    @Movement.performed += instance.OnMovement;
                    @Movement.canceled += instance.OnMovement;
                    @Rotation.started += instance.OnRotation;
                    @Rotation.performed += instance.OnRotation;
                    @Rotation.canceled += instance.OnRotation;
                    @Interaction.started += instance.OnInteraction;
                    @Interaction.performed += instance.OnInteraction;
                    @Interaction.canceled += instance.OnInteraction;
                    @Escape.started += instance.OnEscape;
                    @Escape.performed += instance.OnEscape;
                    @Escape.canceled += instance.OnEscape;
                    @SecondaryInteraction.started += instance.OnSecondaryInteraction;
                    @SecondaryInteraction.performed += instance.OnSecondaryInteraction;
                    @SecondaryInteraction.canceled += instance.OnSecondaryInteraction;
                }
            }
        }
        public InGameActions @InGame => new InGameActions(this);
        private int m_KeyboardAndMouseSchemeIndex = -1;
        public InputControlScheme KeyboardAndMouseScheme
        {
            get
            {
                if (m_KeyboardAndMouseSchemeIndex == -1) m_KeyboardAndMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardAndMouse");
                return asset.controlSchemes[m_KeyboardAndMouseSchemeIndex];
            }
        }
        public interface IInGameActions
        {
            void OnMovement(InputAction.CallbackContext context);
            void OnRotation(InputAction.CallbackContext context);
            void OnInteraction(InputAction.CallbackContext context);
            void OnEscape(InputAction.CallbackContext context);
            void OnSecondaryInteraction(InputAction.CallbackContext context);
        }
    }
}
